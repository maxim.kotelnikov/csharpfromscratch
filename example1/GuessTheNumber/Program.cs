﻿var number = new Random().Next(10) + 1;
Console.WriteLine("Hello, I guessed the number from 1 to 10. Let's try to guess it.");

var userWon = false;
for(var i=0; i < 5; i++)
{
    var userInput = Console.ReadLine();
    var userNumber = int.Parse(userInput);
    if (userNumber == number)
    {
        Console.WriteLine("You won!");
        userWon = true;
        break;
    }
    else if (userNumber < number)
    {
        Console.WriteLine("My number is greater.");
    }
    else
    {
        Console.WriteLine("My number is less.");
    }
}

if(!userWon)
{
    Console.WriteLine("Let's try another time.");
}

